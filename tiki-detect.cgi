#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

# check if user can access the page
# &can_domain($in{'dom'}) || &error($text{'contact_ecannot'});

my $d;
if ($in{'dom'}) {
    $d = &virtual_server::get_domain($in{'dom'});
}

my @dom_php_info = &tikimanager_get_domain_php_info($d);

if (! $d) {
    &error("Domain not found.");
}

my ($info) = &tikimanager_tiki_info($d);
if (! $info->{'instance_id'}) {
    &error("Domain is not installable.");
}

# Page title, must be first UI thing
&ui_print_header(
    'for ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
    "Detect Tiki", "", undef, 1, 1
);


sub print_ln () {
    my ($str) = @_;
    $str =~ s/\s*$//;
    print "<code>$str</code>\n";
}

&$virtual_server::first_print("Detect Tiki using Tiki Manager..");
print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";
&tikimanager_tiki_detect($info->{'instance_id'}, \&print_ln, $d);
print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

my $domain_php_exec = $dom_php_info[2];
my $domain_php_ver = $dom_php_info[0];
my $instance_php_ver = &get_php_version($info->{'phpexec'});
my $php_exec_info = $info->{'phpexec'};
$php_exec_info =~ s{.*/}{};
my $php_exec_domain = $domain_php_exec;
$php_exec_domain =~ s{.*/}{};
if ($php_exec_domain ne $php_exec_info) {
  &$virtual_server::first_print("Detected PHP CLI version from Tiki Manager does not match the one used by the website.");
  &$virtual_server::second_print("Reported by Tiki Manager: " . $info->{'phpexec'} . " (".$instance_php_ver.")");
  &$virtual_server::second_print("Reported by Virtualmin Website: " . $domain_php_exec . " (".$domain_php_ver.")");
  &$virtual_server::second_print("Updating to use " . $domain_php_exec . " ....");
  &tikimanager_execute_command("instance:edit --instances " . $info->{'instance_id'} . " --no-interaction --phpexec " . $domain_php_exec, undef, $d);
  &$virtual_server::second_print("... done");
}

&ui_print_footer(
    $d ? &virtual_server::domain_footer_link($d) : ( ),
    "index.cgi?dom=$in{'dom'}",
    $text{'index_the_information_page'}
);
