#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

# check if user can access the page
# &can_domain($in{'dom'}) || &error($text{'contact_ecannot'});

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

if (! $d) {
  &error("Domain not found.");
}

my ($info) = &tikimanager_tiki_info($d);
if (! $info->{'instance_id'}) {
  &error("Domain is not installable.");
}

my $rebuild_index = $in{'rebuild_index'};
my $stash = $in{'stash'};

# Page title, must be first UI thing
&ui_print_header(
  'for ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  "Updating Tiki", "", undef, 1, 1
);


sub print_ln () {
  my ($str) = @_;
  $str =~ s/\s*$//;
  print "<code>$str</code>\n";
}

&$virtual_server::first_print("Updating Tiki using Tiki Manager..");
print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";
&tikimanager_tiki_update($d, $info->{'instance_id'}, $info->{'branch'}, $rebuild_index, $stash, \&print_ln);
print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "index.cgi?dom=$in{'dom'}",
  $text{'index_the_information_page'}
);
