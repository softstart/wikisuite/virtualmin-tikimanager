#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

# apt-get install libemail-valid-perl
eval 'use Email::Valid;';

# check if user can access the page
# &can_domain($in{'dom'}) || &error($text{'contact_ecannot'});

my @errors;
$in{'admin_email'} = Email::Valid->address($in{'admin_email'});
if (! $in{'admin_email'}) {
  push(@errors, "Invalid admin email");
}

$in{'sender_email'} = Email::Valid->address($in{'sender_email'});
if (! $in{'sender_email'}) {
  push(@errors, "Invalid sender email");
}

if ($in{'password'} !~ m/^[a-zA-Z0-9*.!@#\$%^&()\[\]:;<>,?\/~_+-=|]{8,32}$/) {
  push(@errors, "Invalid password");
}

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

if (! $d) {
  push(@errors, "Domain not found.");
}

if (! $in{'branch'}) {
  push(@errors, "Branch can't be empty");
}

if (@errors) {
  &error(join('<br/>', @errors));
}

my ($tiki_branches_ref) = tikimanager_get_installable_branches($d);
my %branches = map { $_ => 1 } @$tiki_branches_ref;
if (! exists($branches{$in{'branch'}})) {
  &error("$in{'branch'} is not installable.");
}

# Page title, must be first UI thing
&ui_print_header(
  'at ' . '<a href="https://' . $d->{'dom'} .'" target="_blank">https://' . $d->{'dom'} .'</a>',
  'Installing Tiki', "", undef, 1, 1
);

sub print_ln () {
  my ($str) = @_;
  $str =~ s/\s*$//;
  $str =~ s/</&lt;/;
  $str =~ s/>/&gt;/;
  print "$str\n";
}

&$virtual_server::first_print("Installing Tiki using Tiki Manager..");
print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";
&tikimanager_tiki_install_and_config($d, \%in, \&print_ln);
print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

&$virtual_server::first_print("Setting up cron jobs for Tiki..");
&tikimanager_cron_setup($d);
&$virtual_server::second_print(".. done");
&tikimanager_setup_tikiconfig_file($d);

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "index.cgi?dom=$in{'dom'}",
  $text{'index_the_information_page'}
);