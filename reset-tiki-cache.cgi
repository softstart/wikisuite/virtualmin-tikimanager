#!/usr/bin/perl
use strict;
use warnings;

our (%in);

require './virtualmin-tikimanager-lib.pl';
&ReadParse();

my $d = &virtual_server::get_domain($in{'dom'});
&error("Domain not found") if (!$d);
my $dir = tikimanager_get_var_dir();
my $file = "$dir/$in{'type'}.cache";
unlink_file($file) if (-e $file);
redirect("index.cgi?dom=$in{'dom'}");
